// import {Fragment} from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useState, useEffect} from 'react';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Course from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import {UserProvider} from './UserContext';
import CourseView from './pages/CourseView';

export default function App() {

  const [user, setUser] = useState({id: null, isAdmin: false});

  const unSetUser = () =>{
       localStorage.removeItem("token");
       setUser({id: null, isAdmin: false});
  }

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_URI}/users/profile`,
            {headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }}).then(response => response.json())
          .then(data => {
            setUser({id:data._id, isAdmin:data.isAdmin});
          })
  }, []);
  
  return (
    <UserProvider value = {{user,setUser,unSetUser}}>
      <Router>
        <AppNavbar/>
        <Routes>
          <Route path = "*" element = {<Error/>}/>
          <Route path = "/" element = {<Home/>}/>
          <Route path = "/courses" element = {<Course/>}/>
          <Route path = "/courses/:courseId" element = {<CourseView/>}/>
          <Route path = "/login" element = {<Login/>}/>
          <Route path = "/register" element = {<Register/>}/>
          <Route path = "/logout" element = {<Logout/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );

}
