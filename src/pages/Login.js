import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2'

import UserContext from '../UserContext';

export default function Login(){
	
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [isEnabled, setIsEnabled] = useState(false);

	// const [user, setUser] = useState(localStorage.getItem("email"));

	const {user, setUser} = useContext(UserContext);


	useEffect(()=>{
			if(email === "" && password1 === ""){
				setIsEnabled(true);
			}else{
				setIsEnabled(false);
			}
		}, [email, password1])


	function loginUser(event){
		event.preventDefault();
		fetch(`${process.env.REACT_APP_URI}/users/login`, {
			method: "POST",
			headers:{
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
					email: email,
					password: password1
			})
		}).then(response => response.json())
		.then(data =>{
			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to our website!"
				});
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please try again!"
				});
				setPassword1('');
			}
		});

		const retrieveUserDetails = (token) =>{
			fetch(`${process.env.REACT_APP_URI}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}}).then(response => response.json())
			.then(data => {
				setUser({id:data._id, isAdmin:data.isAdmin});
			});
		}
	}

	return(

		(user.id !== null) ?
			<Navigate to = "/"/>
			:
			<Row className = "w-100">
				<Col className = "col-md-4 col-8 mx-auto my-5">
					<Form onSubmit = {loginUser} className = "bg-secondary p-3">
					     <Form.Group className="mb-3" controlId="email">
					       <Form.Label>Email address</Form.Label>
					       <Form.Control type="email" placeholder="Enter email" value = {email} onChange = {event => setEmail(event.target.value)} required/>
					     </Form.Group>

					     <Form.Group className="mb-3" controlId="password1">
					       <Form.Label>Password</Form.Label>
					       <Form.Control type="password" placeholder="Password" value = {password1} onChange = {event => setPassword1(event.target.value)} required/>
					     </Form.Group>

					     <Button variant="primary" disabled = {isEnabled} type="submit">
					       Login
					     </Button>

					</Form>
				</Col>
			</Row>
		)
}