import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2'

import {useState, useEffect,useContext} from 'react';

import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Register(){

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isEnabled, setIsEnabled] = useState(false);

	useEffect(() =>{
		if((email === '' || password1 === '' || password2 === '' || firstName === '' || lastName === '' || mobileNo === '' || mobileNo.length < 11) 
			|| (password1 !== password2)){
			setIsEnabled(true);
		}else{
			setIsEnabled(false);
		}
	}, [email, password1, password2, firstName, lastName, mobileNo]);

	function registerUser(event){
		event.preventDefault();
			fetch(`${process.env.REACT_APP_URI}/users/register`, {
				method: "POST",
				headers:{
					'Content-Type' : 'application/json',
				},
				body: JSON.stringify({
						email: email,
						password: password1,
						firstName : firstName,
						lastName: lastName,
						mobileNo: mobileNo
				})
			}).then(response => response.json())
			.then(data =>{
				console.log(data);
				if(data === true){
					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Welcome to our website!"
					});
					loginUser();
				}else{
					Swal.fire({
						title: "Registration Failed, email exists",
						icon: "error",
						text: "Please try again!"
					});
				}
			});
	}

	function loginUser(){
		fetch(`${process.env.REACT_APP_URI}/users/login`, {
			method: "POST",
			headers:{
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
					email: email,
					password: password1
			})
		}).then(response => response.json())
		.then(data =>{
			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please try again!"
				});
				setPassword1('');
			}
		});

		const retrieveUserDetails = (token) =>{
			fetch(`${process.env.REACT_APP_URI}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}}).then(response => response.json())
			.then(data => {
				setUser({id:data._id, isAdmin:data.isAdmin});
			});
		}
	}

	return(

		(user.id !== null) ?
		<Navigate to = "/"/>
		:
			<Row className = "w-100">
				<Col className = "col-md-4 col-8 mx-auto my-5">
					<Form onSubmit = {registerUser} className = "bg-secondary p-3">
					     <Form.Group className="mb-3" controlId="email">
					       <Form.Label>Email address</Form.Label>
					       <Form.Control type="email" placeholder="Enter Email" value = {email} onChange = {event => setEmail(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="firstName">
					       <Form.Label>First Name</Form.Label>
					       <Form.Control type="text" placeholder="Enter First Name" value = {firstName} onChange = {event => setFirstName(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="lastName">
					       <Form.Label>Last Name</Form.Label>
					       <Form.Control type="text" placeholder="Enter Last Name" value = {lastName} onChange = {event => setLastName(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="mobileNo">
					       <Form.Label>Mobile No.</Form.Label>
					       <Form.Control type="number" placeholder="Enter Mobile Number" value = {mobileNo} onChange = {event => setMobileNo(event.target.value)} required/>
					     </Form.Group>

					     <Form.Group className="mb-3" controlId="password1">
					       <Form.Label>Password</Form.Label>
					       <Form.Control type="password" placeholder="Password" value = {password1} onChange = {event => setPassword1(event.target.value)} required/>
					     </Form.Group>
					     <Form.Group className="mb-3" controlId="password2">
					       <Form.Label>Verify Password</Form.Label>
					       <Form.Control type="password" placeholder="Password" value = {password2} onChange = {event => setPassword2(event.target.value)} required/>
					     </Form.Group>
					     <Button variant="primary" disabled = {isEnabled} type="submit">
					       Register
					     </Button>
					</Form>
				</Col>
			</Row>
		)
}