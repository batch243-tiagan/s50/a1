import {Fragment, useEffect, useState} from 'react'
import CourseCard from '../components/CourseCards';
import courseData from '../data/courses.js';
import {Row} from "react-bootstrap";

export default function Courses(){

	const [courses, setCourses] = useState([]);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
		.then(response => response.json())
		.then(data =>{
			setCourses(data.map(course => {
		return(
				<CourseCard key = {course._id} courseProp = {course}/>
			)
			}))
		})
	},[]);

	return(
		<Fragment>
			<Row className = "m-3">
				{courses}
			</Row>
		</Fragment>
		)
}