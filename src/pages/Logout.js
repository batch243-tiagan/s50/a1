import UserContext from '../UserContext';
import {Navigate } from "react-router-dom";
import {useContext,useEffect} from 'react';
import Swal from 'sweetalert2'

export default function Logout() {
	const context = useContext(UserContext);
	useEffect(() => {
	   context.unSetUser();
	  }, []);
	Swal.fire({
		title: "Welcome back to the outside World!",
		icon: "info",
		text: "Babalik ka rin!"
	});
	return(
			<Navigate to = "/login"/>
		)
}