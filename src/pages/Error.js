import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';
import {Link, Navigate} from 'react-router-dom';

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">  
          <Spinner className = "mx-3"animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
          </Spinner>
          ERROR 404
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
          dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac
          consectetur ac, vestibulum at eros.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button as = {Link} to = "/">Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default function Error(){
	const [modalShow, setModalShow] = React.useState(true);

	  return (
	    <>
	      <MyVerticallyCenteredModal
	        show={modalShow}
	        onHide={() => <Navigate to = "/"/>}
	      />
	    </>
	  );
}
