import Button from "react-bootstrap/Button";
// Bootstrap Grid system
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Link} from 'react-router-dom';

import '../App.css';

export default function Banner(){
	return(
			<Row>
				<Col>
					<h1> Zuiit Coding Bootcamp </h1>
					<p> Opportunities for everyone, everywhere.</p>

					<Button as = {Link} to = "/Courses"> Enroll now! </Button>
				</Col>
			</Row>
		)
}