import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
			<Row className = "my-3">
				<Col xs={12} md={4}>
					<Card className = "h-100 p-3">
					      <Card.Body>
					        <Card.Title>
					        	<h2>Learn From Home</h2>
					        </Card.Title>
					        <Card.Text>
					          	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus vehicula dolor, eget rhoncus est mollis et. Nulla vel viverra augue. Maecenas rutrum tincidunt nulla, vel imperdiet diam. In posuere nisl eget laoreet vehicula. Duis dapibus mi quis sem maximus, eget feugiat libero vulputate. Pellentesque tincidunt, tellus eget ultricies malesuada, tellus nulla aliquet felis, tristique tincidunt urna est in massa. Sed lobortis, neque non dictum sollicitudin, justo libero iaculis augue, id porta dolor lorem vestibulum dui. Duis vel feugiat velit. Duis luctus volutpat felis non aliquet. Praesent mattis tellus tincidunt aliquam vestibulum. Nullam aliquet viverra tellus, fringilla cursus mauris vulputate eget.
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className = "h-100 p-3">
					      <Card.Body>
					        <Card.Title>
					        	<h2>Study Now, Pay Later</h2>
					        </Card.Title>
					        <Card.Text>
					          	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus vehicula dolor, eget rhoncus est mollis et. Nulla vel viverra augue. Maecenas rutrum tincidunt nulla, vel imperdiet diam. In posuere nisl eget laoreet vehicula. Duis dapibus mi quis sem maximus, eget feugiat libero vulputate. Pellentesque tincidunt, tellus eget ultricies malesuada, tellus nulla aliquet felis, tristique tincidunt urna est in massa. Sed lobortis, neque non dictum sollicitudin, justo libero iaculis augue, id porta dolor lorem vestibulum dui. Duis vel feugiat velit. Duis luctus volutpat felis non aliquet. Praesent mattis tellus tincidunt aliquam vestibulum. Nullam aliquet viverra tellus, fringilla cursus mauris vulputate eget.
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className = "h-100 p-3">
					      <Card.Body>
					        <Card.Title>
					        	<h2>Be part of our community</h2>
					        </Card.Title>
					        <Card.Text>
					          	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean maximus vehicula dolor, eget rhoncus est mollis et. Nulla vel viverra augue. Maecenas rutrum tincidunt nulla, vel imperdiet diam. In posuere nisl eget laoreet vehicula. Duis dapibus mi quis sem maximus, eget feugiat libero vulputate. Pellentesque tincidunt, tellus eget ultricies malesuada, tellus nulla aliquet felis, tristique tincidunt urna est in massa. Sed lobortis, neque non dictum sollicitudin, justo libero iaculis augue, id porta dolor lorem vestibulum dui. Duis vel feugiat velit. Duis luctus volutpat felis non aliquet. Praesent mattis tellus tincidunt aliquam vestibulum. Nullam aliquet viverra tellus, fringilla cursus mauris vulputate eget.
					        </Card.Text>
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		)
}