// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';

import {Container, Nav, Navbar} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import '../App.css'
import {Fragment, useContext} from 'react';
import UserContext from '../UserContext'

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	
	return (
			<Navbar className="navbar sticky-top vw-100" bg="light" expand="lg">
			      <Container>
			        <Navbar.Brand as = {NavLink} to = "/">Course Booking</Navbar.Brand>
			        <Navbar.Toggle aria-controls="basic-navbar-nav" />
			        <Navbar.Collapse id="basic-navbar-nav">
			          <Nav className="ms-auto">
			            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
			            <Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>
			  			{
			  				(user.id !== null) ? 
				            	<Nav.Link as= {NavLink} to ="/logout">Logout</Nav.Link>
				            	:
					            <Fragment>
					            	<Nav.Link as = {NavLink} to ="/register">Register</Nav.Link>
					            	<Nav.Link as = {NavLink} to ="/login">Login</Nav.Link>
					            </Fragment>
			  			}
			          </Nav>
			        </Navbar.Collapse>
			      </Container>
			    </Navbar>
		)
}