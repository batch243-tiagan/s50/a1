import {useState, useEffect, useContext} from 'react';
import {Button,Card,Col} from "react-bootstrap";
import UserContext from "../UserContext";
import {Link} from "react-router-dom";

export default function CourseCard({courseProp}) {

	const {_id, name, description, price, slots} = courseProp;

	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots)

	const [isAvailable, setIsAvailable] = useState(true);

	const {user} = useContext(UserContext);

	useEffect(() => {
		if(slotsAvailable === 0){
			setIsAvailable(false);
		}
	}, [slotsAvailable]);

	function enroll(){
			setEnrollees(enrollees + 1);
			setSlotsAvailable(slotsAvailable - 1);
	}

    return (
	    		<Col xs = {12} md = {4} className = "mx-auto">
		    		<Card>
				      <Card.Body>
			 	        <Card.Title>{name}</Card.Title>
			 	        <Card.Subtitle>Description:</Card.Subtitle>

			 	        <Card.Text>{description}</Card.Text>

			 	        <Card.Subtitle>Price:</Card.Subtitle>       
			 	        <Card.Text>{price}</Card.Text>

			 	        <Card.Subtitle>Enrollees:</Card.Subtitle>
			 	        <Card.Text>{enrollees}</Card.Text>

			 	        <Card.Subtitle>Slots Available:</Card.Subtitle>
			 	        <Card.Text>{slotsAvailable} slots</Card.Text>

			 	        {
			 	        	(user !== null) ?
			 	        	<Button as = {Link} to = {`/courses/${_id}`} variant="primary" onClick = {enroll} disabled = {!isAvailable} >Details</Button>
			 	        	:
			 	        	<Button as =  {Link} to = "/login" variant="primary" disabled = {!isAvailable} >Details</Button>
			 	        }

				      </Card.Body>
				    </Card>
				</Col>
    	)
}